# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda";

  # networking.hostName = "nixos"; # Define your hostname.
  networking.hostId = "15c3f604";
  # networking.wireless.enable = true;  # Enables wireless.
  time.timeZone = "America/Chicago";

  environment.systemPackages = with pkgs; [
    xclip
    vim
    git
    emacs
    opam
    coq
    leiningen
    nodejs
    postgresql
    zsh
    (with haskellPackages; [
      ghc
      cabalInstall
      Agda
      idris
      hasktags
      hoogle
      ghcMod
      stylishHaskell
      hlint
      pointfree
      happy
      alex
      present
      purescript
      cabal2nix
      gitAnnex
    ])
  ];

  services.xserver = {
    enable = true;
    layout = "us";
    windowManager.xmonad.enable = true;
    windowManager.xmonad.enableContribAndExtras = true;
    windowManager.xmonad.extraPackages = self : [ self.xmonadContrib self.xmobar ];
    windowManager.default = "xmonad";
    desktopManager.default = "none";
    displayManager.lightdm.enable = true;
    desktopManager.xterm.enable = false;
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # virtual box
  services.virtualboxGuest.enable = true;
  boot.initrd.checkJournalingFS = false;

  users.extraUsers.erichaberkorn = {
    name = "erichaberkorn";
    group = "users";
    uid = 1000;
    createHome = true;
    home = "/home/erichaberkorn";
    shell = "/var/run/current-system/sw/bin/zsh";
  };

  users.defaultUserShell = "/var/run/current-system/sw/bin/zsh";
  programs.zsh.enable = true;

}
